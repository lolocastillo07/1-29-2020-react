import React from 'react';

// function App() {

// 	const[friends,setFriends] = useState([
// 			{
// 				name: "Lorence",
// 				age: 23,
// 				id: Math.Random()

// 			},

// 			{
// 				name: "Joker",
// 				age: 35,
// 				id: Math.Random()

// 			},
			


// 		])

// 		const addFriend = friend => {
// 			setFriends([
// 				...friends,
// 					{name: friend.name, age: friend.age, id: Math.Random()}
// 				])
// 		}


// 	return(

// 		<React.Fragment>

// 			<h1> Hello Telephone </h1>

// 		</React.Fragment>

// 		);
// }	

import AddFriend from './components/AddFriend';
import Friend from './components/Friend';
import uuid from 'uuid/v1';
import EditFriend from './components/EditFriend';

class App extends React.Component{

 state = {
	   friends : [
		   {
			     name:"Ivana",
			     age: 24,
			     id: uuid()
			   },
			   {
			     name:"Nadine",
			     age: 23,
			     id: uuid()
		   	}
		  ]
	 }

	 handleAddFriend = (friend) => {
	 	friend.id = uuid()

	 	this.setState({
	 		friends: [

	 			friend,
	 			...this.state.friends
 			]
 		})
	 }


	 handleEditFriend = (id,updatedFriend) => {
	 	// this.setState({friends.map(friend =>{

	 	// 		return friend.id == id ? updatedFriend : friend
	 	// 	})
	 	// })
		let updatedFriends = this.state.friends.map( friend => {
			return friend.id == id ? updatedFriend : friend
		})
		this.setState({friends : updatedFriends})

		console.log(updatedFriends)
	 }


	 handleDeleteFriend = (id)=>{
	 	// filter
	 	let updatedFriends = this.state.friends.filter(friend =>{
	 		return friend.id !== id
	 	});

	 	this.setState({
	 		friends: updatedFriends
	 	})

	 }

	 render(){
	   		return(
		     <React.Fragment>
{/*			       <h1>
			         "Hello AFRICA"
			       </h1>*/}
		     <AddFriend addFriend={this.handleAddFriend}/>
		     <Friend 
		       friends={this.state.friends} 
		       deleteFriend = {this.handleDeleteFriend}
		       />
		     <EditFriend 
		     friends={this.state.friends}
		     editFriend={this.handleEditFriend}

		     />
		     </React.Fragment>
	   )
	 }
}

export default App;
