import React from 'react';

class AddFriend extends React.Component {

	state = {

		friend : {
			name : null,
			age: null
		}
	}



	handleChange = (e)=>{
		// console.log(e.target.id)

		this.setState({ [e.target.id] : e.target.value })
	}

	handleSubmit=(e)=>{
		e.preventDefault()
		// console.log(this.props)
		this.props.addFriend(this.state)

		this.setState({name: null, age: null})
		e.target.reset()
	}



	render(){
		return(
			<div className='bg-dark text-white'>
				<h1>Let's Collect GF Pare!</h1>
				<form onSubmit = {this.handleSubmit}>
					Name:
						<input onChange={this.handleChange} id='name' type='text'/>
					Age:
						<input onChange={this.handleChange} id='age' type='number'/>
						<button>Add GF</button>
				</form>
			</div>
		)
	}
}

export default AddFriend;