import React from 'react';

class Friend extends React.Component {





render(){
		return (
			<div className="bg-primary">
				<ul>
	{	
				this.props.friends.map(friend=>{
				return(
					<li key={friend.id}>
						My Girlfriend {friend.name} is {friend.age} years old
						<button onClick={ ()=>{this.props.deleteFriend(friend.id)}}>
						&times;
						</button>
					</li>
				)
				})
}
				</ul>
			</div>
		);
	}
}

export default Friend;